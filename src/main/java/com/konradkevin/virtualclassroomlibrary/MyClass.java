package com.konradkevin.virtualclassroomlibrary;

import com.google.firebase.auth.FirebaseAuth;

public class MyClass {
    FirebaseAuth auth;

    public FirebaseAuth getAuth() {
        if (auth == null) {
            auth = FirebaseAuth.getInstance();
        }

        return auth;
    }
}
