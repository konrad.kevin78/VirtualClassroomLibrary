package com.konradkevin.virtualclassroomlibrary;

public interface IIterable {
    public IIterator getIterator();
}
